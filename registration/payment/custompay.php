<?php
session_start();
$txt = '\n\n========================='.date("m/d/Y h:i:s a", time()).'===================\n'.json_encode($_POST).json_encode($_COOKIE['which']);
$myfile = file_put_contents('logsinitiatepay.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
?>
<!DOCTYPE html>
<html>
<head>
    <title>ISEC 2018 - Payment Details</title>
    <meta charset="utf-8">
    <style type='text/css'>
        input {
            font-family: Verdana, Arial, sans-serif;
            color: #08185A;
            border: 2px solid #003156;
            padding: .5em;
            background-color: #E1E1E1;
            font-weight: bold;
        }

        input[type=submit] {
            cursor: pointer;
        }

        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        #pagetitle {
            background-color: #003156;
            color: #FFFFFF;
            font-size: 1.4em;
            padding: .4em;
            text-align: center;
            font-family: serif;
        }

        .container {
            max-width: 1080px;
            margin: auto;
            padding: 1em;
        }

        table {
            margin: auto;
        }

        td {
            padding: 10px;
        }
    </style>
</head>
<body>

<?php

$connection = mysql_connect("localhost", "isoft", "password");
mysql_select_db("isoft_isec18", $connection) or die(mysql_error());
$query = @mysql_query("select max(trans_no) from transID");
$transid = mysql_fetch_array($query);



$id = $transid[0];

// To create uniue id
//$_POST[''];
$paperid = 'custom';
$name = $_POST['name'];
$email = 'shivam.khandelwal@research.iiit.ac.in';
$transid = $paperid.$name.$email;
$transid = 'ISEC'.substr(md5($transid), -7).(time()%1000);
$_SESSION['uniquetransid'] = $transid;

global $values;
$type = $_COOKIE['which'];
$values = Array(
        'regular-before-IN' => 7500,
        'regular-after-IN' => 8500,
        'academic-before-IN' => 5500,
        'academic-after-IN' => 6500,
        'student-before-IN' => 3000,
        'student-after-IN' => 4000,
        'regular-before-OIN' => 300,
        'regular-after-OIN' => 350,
        'academic-before-OIN' => 250,
        'academic-after-OIN' => 300,
        'student-before-OIN' => 150,
        'student-after-OIN' => 175,
        'regular-complementary-IN' => 0,
    );

$Passval = $_GET['Passval'];
$vals = explode("*", $Passval);


$_SESSION['payeename'] = $name;
$amt = 1;
$amt = $amt * 1.18; //GST
$desig = $_POST['paperid'].' ** '.$_POST['papertitle'];
$place = $_POST['address'].', '.$_POST['city'].', '.$_POST['country'].' -- '.$_POST['zip'];
$affliated = $email;
$paytype = $type;
$_SESSION['email'] = $email;
$_COOKIE['email'] = $email;

$country = substr($type, -3);

if ($country == "OIN") {
    $inrvalue = 65;
    $amt = $amt * $inrvalue;
}


$sql = "CREATE TABLE IF NOT EXISTS information (";
$insert = "INSERT INTO information VALUES (";
foreach($_POST as $key=>$value) {
    if($key == 'offline') {
        continue;
    }
    $sql .= $key." VARCHAR(1000), ";
    $insert .= "'".$value."',";
}
$sql .= "transid VARCHAR(100));";
$insert .= "'$transid');";
// echo $insert;
$query = @mysql_query($sql);
$query = @mysql_query($insert);

$date = date("d.m.y");
$query = @mysql_query("insert into persondetails values('$transid','$name','$desig','$place','$affliated','$amt','$date','$paytype')");

?>
<div id="pagetitle">
    ISEC 2018 : 11th Innovations in Software Engineering Conference
    <br/>
    <small style="font-size: 0.5em;">IIIT-H, Hyderabad, India</small>
</div>

<div class="container">
    <?php
    $action = "./vpc_php_serverhost_do.php";
if(isset($_POST['offline'])) {
    $action = "#";
}
?>
    <form action="<?php echo $action; ?>" method="post">
        <input type="hidden" name="Title" value="PHP VPC 3 Party Transacion">
        <input type="hidden" name="virtualPaymentClientURL" size="63" value="https://migs.mastercard.com.au/vpcpay"> <!-- Virtual Payment Client URL -->
        <input type="hidden" name="vpc_Version" value="1"> <!-- VPC Version -->
        <input type="hidden" name="vpc_Command" value="pay"> <!-- Command Type -->
        <input type="hidden" name="vpc_AccessCode" value="A04CF7F2"> <!-- Merchant AccessCode -->
        <input type="hidden" name="vpc_Merchant" value="IIITHYD"> <!-- MerchantID -->
        <input type="hidden" name="vpc_OrderInfo" value="ISEC18"> <!-- Transaction OrderInfo -->
        <input type="hidden" name="vpc_Locale" value="en_AU"><!-- Payment Server Display Language Locale | en_AU -->
        <input type="hidden" name="vpc_ReturnURL" value="https://isoft.acm.org/isec2018/payment/vpc_php_serverhost_dr.php"> <!-- Receipt ReturnURL -->
        <input type="hidden" name="vpc_Currency" value="INR" /> <!-- Currency -->
        <table>
            <tr>
<?php
if(isset($_POST['offline'])) {
?>
            <th>
                    Please contact <a href="mailto:isec2018Secretariat@iiit.ac.in">isec2018Secretariat@iiit.ac.in</a> with transaction reference number below for offline payment.

            </th>
        </table>
        <table>
<?php
}
else if($amt < 1) {
?>
            <th>
                    Your registration is successful.
            </th>
        </table>
        <table>
<?php
}
else {
?>
            <th colspan="2">
                    Verify the Information & Proceed
                </th>

<?php
}
?>            </tr>
            <tr>
                <td>
                    Name
                </td>
                <td>
                    <input type="text" value="<?php echo $name; ?>" >
                </td>
            </tr>
            <tr>
                <td>
                    Transaction Reference Number
                </td>
                <td>
                    <input type="text" name="vpc_MerchTxnRef" value="<?php echo $transid; ?>" >
                </td>
            </tr>
            <tr>
                <td>
                    Purchase Amount: (in INR)
                </td>
                <td>
                    <input type="text" name="vpc_Amount" value="<?php echo $amt; ?>" size="20" maxlength="10">

        <input type="text" name="vpc_Am2ount2222" value="<?php echo $amt; ?>" size="20" maxlength="10">
        </td>
            </tr>
<?php
if(!isset($_POST['offline']) && $amt>0) {
?>
            <tr>
                <th colspan="2">
                    <input type="submit" name="SubButL" value="Pay Now!">
                </th>
            </tr>
<?php 
}
?>
        </table>
    </form>
</div>
</body>
</html>
