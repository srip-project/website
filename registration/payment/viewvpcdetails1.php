<html>
    <head>
        <title><?=$title?> - <?=$errorTxt?>Response Page</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
        <style type="text/css">
            <!--
            h1       { font-family:Arial,sans-serif; font-size:24pt; color:#08185A; font-weight:100}
            h2.co    { font-family:Arial,sans-serif; font-size:24pt; color:#08185A; margin-top:0.1em; margin-bottom:0.1em; font-weight:100}
            h3.co    { font-family:Arial,sans-serif; font-size:16pt; color:#000000; margin-top:0.1em; margin-bottom:0.1em; font-weight:100}
            body     { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#08185A background-color:#FFFFFF }
            p        { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#FFFFFF }
            a:link   { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#08185A }
            a:visited{ font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#08185A }
            a:hover  { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#FF0000 }
            a:active { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#FF0000 }
			tr       { height:25px; }
			tr.shade { height:25px; background-color:#E1E1E1 }
			tr.title { height:25px; background-color:#C1C1C1 }
            td       { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#08185A }
            td.red   { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#FF0066 }
            td.green { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#00AA00 }
            th       { font-family:Verdana,Arial,sans-serif; font-size:10pt; color:#08185A; font-weight:bold; background-color:#E1E1E1; padding-top:0.5em; padding-bottom:0.5em}
            input    { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#08185A; background-color:#E1E1E1; font-weight:bold }
            select   { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#08185A; background-color:#E1E1E1; font-weight:bold; width:463 }
            textarea { font-family:Verdana,Arial,sans-serif; font-size:8pt; color:#08185A; background-color:#E1E1E1; font-weight:normal; scrollbar-arrow-color:#08185A; scrollbar-base-color:#E1E1E1 }
            -->
        </style>
    </head>
    <body>

		<!-- start branding table -->
		<table width='100%' border='2' cellpadding='2' bgcolor='#C1C1C1'>
		<tr>
	  	<td bgcolor='#E1E1E1' width='90%'><h2 class='co'>&nbsp;Payment Client</h2></td>
			<td bgcolor='#C1C1C1' align='center'><h3 class='co'>MIGS</h3></td>
	     </tr>
		</table>
<?php

$connection=mysql_connect("localhost","root","ltrcicon");
mysql_select_db("utitransaction",$connection) or die(mysql_error());

$id=$_REQUEST['id'];
$query=@mysql_query("select * from requestvalues where transactionID in('$id')");
while($row=mysql_fetch_array($query))
{
$vpcversion=$row['vpc_API_version'];
$Command=$row['Command'];
$txnref=$row['Merchant_trans_ref'];
$amt=$row['purchase_amt'];
}

?><br><br>
     <!-- <table width='85%' border='2' align="center" cellpadding='2' bgcolor='#C1C1C1'>-->

             <table width="85%" border=1 align="center" cellpadding="5" border="0">
            <tr class="title">
                <td  colspan="2" height="25"><P><strong>&nbsp;<b>Basic Transaction Fields</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></P></td>
            </tr>
            <tr>
                <td align="right" width="55%"><strong><i>VPC API Version: </i></strong></td>
                <td width="45%"><?=$vpcversion?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Command: </i></strong></td>
                <td><?=$Command?></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Merchant Transaction Reference: </i></strong></td>
                <td><?=$txnref?></td>
             </tr>
             <tr class="shade">
                <td align="right"><strong><i>Purchase Amount: </i></strong></td>
                <td><?=$amt ?></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <font color="#C1C1C1">Fields above are the request values returned.<br />
                    <HR />
                    Fields below are the response fields for a Standard Transaction.<br /></font>
                </td>
            </tr>
<?php
$id=$_REQUEST['id'];
$query=@mysql_query("select * from responsevalues where transactionID in('$id')");
while($row=mysql_fetch_array($query))
{
$txncode=$row['vpc_trans_response_code'];
$txncodedesc=$row['trans_response_code_desc'];
$msg=$row['message'];
$receiptno=$row['Receipt_number'];
$txnno=$row['Transaction_number'];
$rescode=$row['Acquirer_Response_Code'];
$authorid=$row['Bank_authorizationID'];
$batchno=$row['Batch_number'];
$cardtype=$row['Card_type'];
}

?>
 
            <tr class="shade">
                <td align="right"><strong><i>VPC Transaction Response Code: </i></strong></td>
                <td><?=$txncode?></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Transaction Response Code Description: </i></strong></td>
                <td><?=$txncodedesc?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Message: </i></strong></td>
                <td><?=$msg?></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Receipt Number: </i></strong></td>
                <td><?=$receiptno?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Transaction Number: </i></strong></td>
                <td><?=$txnno?></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Acquirer Response Code: </i></strong></td>
                <td><?=$rescode?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Bank Authorization ID: </i></strong></td>
                <td><?=$authorid?></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Batch Number: </i></strong></td>
                <td><?=$batchno?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Card Type: </i></strong></td>
                <td><?=$cardtype?></td>
            </tr>
       </table>
    </body>
</html>
