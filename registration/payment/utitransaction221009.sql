-- MySQL dump 8.23
--
-- Host: localhost    Database: utitransaction
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `credit_details`
--

CREATE TABLE credit_details (
  email varchar(50) NOT NULL default '',
  name varchar(40) default NULL,
  number varchar(16) default NULL,
  type varchar(10) default NULL,
  expir_month varchar(10) default NULL,
  expir_year int(11) default NULL,
  bill_address varchar(250) default NULL,
  city varchar(30) default NULL,
  state varchar(20) default NULL,
  country varchar(40) default NULL,
  trans_date date default NULL,
  amount int(11) default NULL,
  PRIMARY KEY  (email)
) TYPE=MyISAM;

--
-- Dumping data for table `credit_details`
--



--
-- Table structure for table `late_pay`
--

CREATE TABLE late_pay (
  del_name varchar(250) default NULL,
  card_name varchar(250) default NULL,
  card_no varchar(25) default NULL,
  email varchar(200) default NULL,
  amount varchar(50) default NULL
) TYPE=MyISAM;

--
-- Dumping data for table `late_pay`
--



--
-- Table structure for table `persondetails`
--

CREATE TABLE persondetails (
  transid varchar(30) default NULL,
  name varchar(25) default NULL,
  desig varchar(30) default NULL,
  place varchar(30) default NULL,
  affiliated varchar(35) default NULL,
  Amount varchar(30) default NULL,
  date varchar(15) default NULL,
  mode varchar(10) default NULL
) TYPE=MyISAM;

--
-- Dumping data for table `persondetails`
--


INSERT INTO persondetails VALUES ('icon13','Meenakshi Sundaram Muruge','3','Research Scholar,Department of','Anna University','1500','23.10.08','inr');
INSERT INTO persondetails VALUES ('icon','Meenakshi Sundaram Muruge','3','Research Scholar,Department of','Anna University','1500','23.10.08','inr');
INSERT INTO persondetails VALUES ('icon1','Meenakshi Sundaram Muruge','3','Research Scholar,Department of','Anna University','1500','23.10.08','inr');
INSERT INTO persondetails VALUES ('icon1','RAMBABU PAIDIMARRI','1','GachibowliHyderabad','IIIT-Hyderabad','3000','24.10.08','inr');
INSERT INTO persondetails VALUES ('icon2','RAMBABU PAIDIMARRI','1','GachibowliHyderabad','IIIT-Hyderabad','3000','24.10.08','dollar');
INSERT INTO persondetails VALUES ('icon2','Paidimarri Rambabu','2','Gachibowli, Hyderabad','IIIT-Hyderabad','10000','24.10.08','dollar');
INSERT INTO persondetails VALUES ('icon2','Meenakshi Sundaram Muruge','3','Research Scholar,Department of','Anna University','1500','28.10.08','inr');
INSERT INTO persondetails VALUES ('icon3','Meenakshi Sundaram Muruge','3','Research Scholar,Department of','Anna University','1500','28.10.08','inr');
INSERT INTO persondetails VALUES ('icon3','afsfah dgsg','3','asfhasfhshasfhd','ssdfhfsa','1500','29.10.08','dollar');
INSERT INTO persondetails VALUES ('icon4','afsfah dgsg','3','asfhasfhshasfhd','ssdfhfsa','1500','29.10.08','dollar');
INSERT INTO persondetails VALUES ('icon4','','','','','0','29.10.08','inr');
INSERT INTO persondetails VALUES ('icon4','fgh','','','','0','29.10.08','inr');
INSERT INTO persondetails VALUES ('icon4','fgh','','','','3000','29.10.08','inr');
INSERT INTO persondetails VALUES ('icon4','ghj gj','1','ghj','ghj','3000','30.10.08','inr');
INSERT INTO persondetails VALUES ('icon5','ghj gj','1','ghj','ghj','3000','30.10.08','inr');
INSERT INTO persondetails VALUES ('icon5','subbiah shyamala','1','klhjk','IIITH','7522.5','04.11.08','');
INSERT INTO persondetails VALUES ('icon5','Monojit Choudhury','2','Scientia, 196/36, 2nd Main Sad','Microsoft Research India','6000','07.11.08','inr');
INSERT INTO persondetails VALUES ('icon6','Monojit Choudhury','2','Scientia, 196/36, 2nd Main Sad','Microsoft Research India','6000','09.11.08','inr');
INSERT INTO persondetails VALUES ('icon7','Nisheeth Joshi','1','AIM','Banasthali University','750','09.11.08','inr');
INSERT INTO persondetails VALUES ('icon7','Ramanand Janardhanan','2','6, Amal Apartments, Baner Road','Cognizant Technology Solutions','6000','09.11.08','inr');
INSERT INTO persondetails VALUES ('icon8','Plaban Bhowmick','3','Dept. Of Computer Science','Indian Institute of Technology Khar','1500','10.11.08','inr');
INSERT INTO persondetails VALUES ('icon9','BISWANATH BARIK','3','E-201, VSRC, IIT Kharagpur.Wes','IIT Kharagpur','1500','12.11.08','inr');
INSERT INTO persondetails VALUES ('icon9','Niraj Shrestha','3','Dhulikhel, Kavre','Kathmandu University','3750','12.11.08','dollar');
INSERT INTO persondetails VALUES ('icon9','Doren Singh Thoudam','1','Gulmohor Cross Road No 9Juhu,','CDAC Mumbai','750','12.11.08','inr');
INSERT INTO persondetails VALUES ('icon13','SHIVA KUMAR H R','3','MILE (Medical Intelligence and','Indian Institute of Science (IISc)','1500','12.11.08','inr');
INSERT INTO persondetails VALUES ('icon11','','','','','0','12.11.08','');
INSERT INTO persondetails VALUES ('icon11','Niraj Shrestha','3','Dhulikhel, Kavre','Kathmandu University','3750','12.11.08','dollar');
INSERT INTO persondetails VALUES ('icon11','Rohit Shrestha','3','kathmandu, Nepal','Purwanchal univresity','3750','12.11.08','inr');
INSERT INTO persondetails VALUES ('icon11','Rohit Shrestha','3','kathmandu, Nepal','Purwanchal univresity','3750','12.11.08','inr');
INSERT INTO persondetails VALUES ('icon12','gdfg gf','3','dgdfgd','dfg','400','12.11.08','inr');
INSERT INTO persondetails VALUES ('icon14','try try','1','rytw','rwty','750','12.11.08','');
INSERT INTO persondetails VALUES ('icon14','Sanjay Chatterji','3','C 301, VSRCIIT KharagpurIndia','IIT Kharagpur','1500','13.11.08','inr');
INSERT INTO persondetails VALUES ('icon15','Sanjay Chatterji','3','C 301, VSRCIIT KharagpurIndia','IIT','1500','13.11.08','inr');
INSERT INTO persondetails VALUES ('icon16','Biswanath Barik','3','E 201, VSRCIIT kharagpurINDIA','IIT Kharagpur','1500','13.11.08','inr');
INSERT INTO persondetails VALUES ('icon17','Sanjay Chatterji','3','C-301, VSRC, IIT Kharagpur, We','IIT Kharagpur','1500','13.11.08','inr');
INSERT INTO persondetails VALUES ('icon18','Kalika Bali','2','Scientia, 196/36, 2nd Main,Sad','Microsoft Research Labs India','6000','13.11.08','inr');
INSERT INTO persondetails VALUES ('icon19','Rabin Mondal','1','dsdsds','sdsds','3750','13.11.08','');
INSERT INTO persondetails VALUES ('icon20','Sujan Kr Saha','3','ZH - 2/305,IIT KharagpurIndia-','IIT Kharagpur','1500','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon21','sitanath biswas','1','plot-761,jaydev viharbhubanesw','ITER,Bhubaneswar','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon21','sitanath biswas','1','plot-761,jaydev viharbhubanesw','ITER,Bhubaneswar','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon21','PARTHA PAKRAY','1','DEPARTMENT OF COMPUTER SCIENCE','JADAVPUR UNIVERSITY','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon23','PARTHA PAKRAY','1','DEPARTMENT OF COMPUTER SCIENCE','JADAVPUR UNIVERSITY','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon24','Dipankar Das','3','Department of Computer Science','Jadavpur University','1900','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon24','PARTHA PAKRAY','1','DEPARTMENT OF COMPUTER SCIENCE','JADAVPUR UNIVERSITY','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon25','Dipankar Das','3','Department of Computer Science','Jadavpur University','1900','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon26','SIVAJI BANDYOPADHYAY','1','CSE Department,Jadavpur Univer','JADAVPUR UNIVERSITY','3000','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon27','S S S Kumar CHIVUKULA','2','First Indian Corporation Pvt L','Industry','6000','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon28','Asif Ekbal','3','188, Raja SC Mallick Road, Jad','Department of Computer Science and','1500','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon29','Asif Ekbal','3','188, Raja SC Mallick Road, Jad','Department of Computer Science and','1500','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon30','Asif Ekbal','3','188, Raja SC Mallick Road, Jad','Department of Computer Science and','1500','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon31','Santanu Pal','1','Department of Computer Science','Jadavpur University','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon31','Santanu Pal','1','Department of Computer Science','Jadavpur University','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon32','Santanu Pal','1','Department of Computer Science','Jadavpur University','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon32','Santanu Pal','1','Department of Computer Science','Jadavpur University','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon33','Santanu Pal','1','Department of Computer Science','Jadavpur University','3750','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon33','Dipankar Das','3','Department of Computer Science','Jadavpur University','1900','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon36','Dipankar Das','3','Department of Computer Science','Jadavpur University','1900','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon37','Dipankar Das','3','Department of Computer Science','Jadavpur University','1900','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon39','Ramanand Janardhanan','2','6, Amal Apartments, Baner Road','Cognizant Technology Solutions','6000','14.11.08','inr');
INSERT INTO persondetails VALUES ('icon40','A. Nayeemulla Khan','2','No 25, 2nd Main Road,Kamaraj N','Acusis India','6000','15.11.08','inr');
INSERT INTO persondetails VALUES ('icon41','A. Nayeemulla Khan','2','No 25, 2nd Main Road,Kamaraj N','Acusis India','6000','15.11.08','inr');
INSERT INTO persondetails VALUES ('icon42','A. Nayeemulla Khan','2','No 25, 2nd Main Road,Kamaraj N','Acusis India','6000','15.11.08','inr');
INSERT INTO persondetails VALUES ('icon43','A.Nayeemulla Khan','2','No 25 2nd Main Road Kamaraj Na','Acusis','6000','15.11.08','');
INSERT INTO persondetails VALUES ('icon44','Arnab Dhar','3','J-302 Paharpur RoadGarden Reac','RCC Institute of Information Techno','1500','15.11.08','');
INSERT INTO persondetails VALUES ('icon44','sitanath biswas','1','plot-761,jaydev viharbhubanesw','SOA University, bhubaneswar','3750','16.11.08','inr');
INSERT INTO persondetails VALUES ('icon45','sitanath biswas','1','plot-761,jaydev viharbhubanesw','SOA University, bhubaneswar','3750','16.11.08','inr');
INSERT INTO persondetails VALUES ('icon46','Cohan Carlos','2','253, I Cross, II Block,Banasha','Microsoft Research','7200','19.11.08','inr');
INSERT INTO persondetails VALUES ('icon47','Cohan Carlos','2','253, I Cross, II Block,Banasha','Microsoft Research','7200','19.11.08','inr');
INSERT INTO persondetails VALUES ('icon47','Cohan Carlos','2','253, I Cross, II Block,Banasha','Microsoft Research','7200','19.11.08','inr');
INSERT INTO persondetails VALUES ('icon48','Tirthankar Dasgupta','3','VSRC, Block-D-102, IIT Kharagp','Indian Institute of Technology','1800','21.11.08','inr');
INSERT INTO persondetails VALUES ('icon48','Shivapratap Gopakumar','1','Senior Reserach AssociateCEN,A','Amrita University','4500','22.11.08','inr');
INSERT INTO persondetails VALUES ('icon49','Shivapratap Gopakumar','1','CEN,Amrita University,Ettimada','Amrita','4500','22.11.08','inr');
INSERT INTO persondetails VALUES ('icon50','Dhanalakshmi V','1','CEN,Amrita University,Ettimada','Amrita University','900','22.11.08','inr');
INSERT INTO persondetails VALUES ('icon51','VIjaya Vijayakumar','1','CEN,Amrita University,Ettimada','Amrita University','900','22.11.08','inr');
INSERT INTO persondetails VALUES ('icon52','A Kumaran','2','Scientia196/36 2nd Main RoadSa','Microsoft Research India','7200','25.11.08','inr');
INSERT INTO persondetails VALUES ('icon52','A Kumaran','2','Scientia196/36 2nd Main RoadSa','Microsoft Research India','7200','25.11.08','inr');
INSERT INTO persondetails VALUES ('icon53','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon53','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon53','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon53','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon53','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon53','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon53','Dandapat Sandipan','2','Microsoft Research India','Microsoft Research India','7200','25.11.08','inr');
INSERT INTO persondetails VALUES ('icon54','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon54','','','','','0','25.11.08','');
INSERT INTO persondetails VALUES ('icon54','A Kumaran','2','ScientiaSadashivnagarBangalore','Microsoft Research India','7200','25.11.08','inr');
INSERT INTO persondetails VALUES ('icon55','Dhirendra Singh','3','Room no-6, Ayush Arched Boys H','Full time students','2300','25.11.08','inr');
INSERT INTO persondetails VALUES ('icon56','Dhirendra Singh','3','Room no-6, Ayush Arched Boys H','Full time students','2300','25.11.08','inr');
INSERT INTO persondetails VALUES ('icon56','A Kumaran','2','ScientiaSadashivnagarBangalore','Microsoft Research India','7200','26.11.08','inr');
INSERT INTO persondetails VALUES ('icon57','Abhijit Payeng','3','School of Translation and Inte','Research Scholar','1800','26.11.08','inr');
INSERT INTO persondetails VALUES ('icon58','Janardan Mishra','3','Room No-6;Ayush Arched Boys Ho','full time students','2300','26.11.08','inr');
INSERT INTO persondetails VALUES ('icon61','SARAVANAN KRISHNAN','2','Microsoft Research India,','MICROSOFT RESEARCH INDIA','7200','26.11.08','inr');
INSERT INTO persondetails VALUES ('icon61','','','','','0','26.11.08','');
INSERT INTO persondetails VALUES ('icon62','Abhijit Payeng','3','School of Translation and Inte','Research Scholar','500','26.11.08','inr');
INSERT INTO persondetails VALUES ('icon63','Abhijit Payeng','3','School of Translation and Inte','Research Scholar','500','26.11.08','inr');
INSERT INTO persondetails VALUES ('icon64','Tirthankar Dasgupta','3','VSRC, Block-D-102, IIT Kharagp','Indian Institute of Technology','1800','27.11.08','inr');
INSERT INTO persondetails VALUES ('icon65','Tirthankar Dasgupta','3','VSRC, Block-D-102, IIT Kharagp','Indian Institute of Technology','1800','27.11.08','inr');
INSERT INTO persondetails VALUES ('icon66','A Kumaran','2','ScientiaSadashivnagarBangalore','Microsoft Research India','7200','27.11.08','inr');
INSERT INTO persondetails VALUES ('icon67','ss ss','3','sgfsdsdfsdfdsf','ss','1800','29.11.08','');
INSERT INTO persondetails VALUES ('icon68','saurabh kumar','3','MAHATMA GANDHI ANTARRASHTRIYA','main conferance','1500','02.12.08','inr');
INSERT INTO persondetails VALUES ('icon69','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','02.12.08','dollar');
INSERT INTO persondetails VALUES ('icon70','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','02.12.08','dollar');
INSERT INTO persondetails VALUES ('icon71','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','02.12.08','dollar');
INSERT INTO persondetails VALUES ('icon72','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','02.12.08','dollar');
INSERT INTO persondetails VALUES ('icon73','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','02.12.08','dollar');
INSERT INTO persondetails VALUES ('icon74','Gobind Prasad','3','Dept.of Language Technology,Ma','University','2300','02.12.08','inr');
INSERT INTO persondetails VALUES ('icon74','Janardan Mishra','3','Room No-6;Ayush Arched Boys Ho','full time students','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon75','Milind Patil','1','Dept.of.translation','University','3600','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon75','Milind Patil','1','Dept.of.translation','University','3600','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon76','Milind Patil','1','Dept.of.translation','University','3600','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon76','Janardan Mishra','3','Room No-6;Ayush Arched Boys Ho','full time students','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon76','Milind Patil','3','Dept.of.Translation and Interp','University','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon78','Milind Patil','3','Dept.of.Translation and Interp','University','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon78','Rahul Mhaiskar','3','Dept of Language TechnologyMah','University','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon79','Rahul Mhaiskar','3','Dept of Language TechnologyMah','University','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon79','subodh kumar','3','Mahatma Gandhi Antarrashtriya','University','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon80','subodh kumar','3','Mahatma Gandhi Antarrashtriya','University','2300','03.12.08','inr');
INSERT INTO persondetails VALUES ('icon80','Milind Patil','1','Dept.of.translation','University','3600','04.12.08','inr');
INSERT INTO persondetails VALUES ('icon80','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','05.12.08','dollar');
INSERT INTO persondetails VALUES ('icon80','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','05.12.08','dollar');
INSERT INTO persondetails VALUES ('icon81','Harsha Wadatkar','3','Dept.of Language Technology,Ma','University','2300','05.12.08','inr');
INSERT INTO persondetails VALUES ('icon81','Janardan Mishra','3','Room No-6;Ayush Arched Boys Ho','full time students','2300','05.12.08','inr');
INSERT INTO persondetails VALUES ('icon81','Janardan Mishra','3','Room No-6;Ayush Arched Boys Ho','full time students','2300','05.12.08','inr');
INSERT INTO persondetails VALUES ('icon82','Charanjeet Singh','3','Mahatma Gandhi Antarrashtriya','University','2300','05.12.08','inr');
INSERT INTO persondetails VALUES ('icon82','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','05.12.08','dollar');
INSERT INTO persondetails VALUES ('icon84','Altaf Mahmud','1','G.P. JA-31/3, Wireless Gate, M','Mr.','9000','06.12.08','dollar');
INSERT INTO persondetails VALUES ('icon85','Pavel Stranak','3','Institute of Formal and Applie','Charles University in Prague','7000','09.12.08','inr');
INSERT INTO persondetails VALUES ('icon85','Anusha Jain','2','NEC HCL ST,Plot No 5,Tower B,','NEC HCL ST','10800','09.12.08','inr');
INSERT INTO persondetails VALUES ('icon86','Parul Nath','2','NEC HCL ST,Plot 5, Tower B,Log','NEC HCL ST','10800','09.12.08','inr');
INSERT INTO persondetails VALUES ('icon87','Anusha Jain','2','NEC HCL ST,Plot No 5,Tower B,','NEC HCL ST','10800','09.12.08','inr');
INSERT INTO persondetails VALUES ('icon88','Pavel Stranak','1','UFAL,Malostranske nam. 25,Prah','Charles University in Prague','13500','09.12.08','inr');
INSERT INTO persondetails VALUES ('icon89','Pavel Stranak','1','UFAL,Malostranske nam. 25,Prah','Charles','13500','09.12.08','dollar');
INSERT INTO persondetails VALUES ('icon90','Pavel Stranak','1','UFAL,Malostranske nam. 25,Prah','Charles','13500','09.12.08','dollar');
INSERT INTO persondetails VALUES ('icon90','Dhirendra Singh','3','Room no-6, Ayush Arched Boys H','Full time students','2300','10.12.08','inr');
INSERT INTO persondetails VALUES ('icon91','satyendra kumar','3','s,3purandra apartment,mahavir','university','2300','10.12.08','inr');
INSERT INTO persondetails VALUES ('icon92','Anjani Ray','','','Not-for-profit R','900','10.12.08','inr');
INSERT INTO persondetails VALUES ('icon93','Leonardo Lesmo','1','C.so Svizzera 18510149 TORINO','University of Torino - Italy','9000','10.12.08','dollar');
INSERT INTO persondetails VALUES ('icon94','Anusha Jain','2','NEC HCL ST,Plot No 5,Tower B,','NEC HCL ST','10800','19.12.08','inr');
INSERT INTO persondetails VALUES ('icon95','Anusha Jain','2','NEC HCL ST,Plot No 5,Tower B,','NEC HCL ST','10800','24.12.08','inr');

--
-- Table structure for table `requestvalues`
--

CREATE TABLE requestvalues (
  transactionID varchar(15) default NULL,
  Date varchar(20) default NULL,
  vpc_API_version varchar(5) default NULL,
  Command varchar(20) default NULL,
  Merchant_trans_ref varchar(50) default NULL,
  Merchant_access_code varchar(20) default NULL,
  Merchant_ID varchar(20) default NULL,
  order_info varchar(20) default NULL,
  purchase_amt varchar(20) default NULL,
  email varchar(250) default NULL
) TYPE=MyISAM;

--
-- Dumping data for table `requestvalues`
--


INSERT INTO requestvalues VALUES ('icon','2008/10/23','1','pay','Meenakshi Sundaram Murugeshan','A04CF7F2','IIITHYD','ICON08','1500','msundar_26@yahoo.com');
INSERT INTO requestvalues VALUES ('icon1','2008/10/24','1','pay','RAMBABU PAIDIMARRI','A04CF7F2','IIITHYD','ICON08','3000','rambabu@iiit.ac.in');
INSERT INTO requestvalues VALUES ('icon2','2008/10/28','1','pay','Meenakshi Sundaram Murugeshan','A04CF7F2','IIITHYD','ICON08','1500','rmurugeshan@hotmail.com');
INSERT INTO requestvalues VALUES ('icon3','2008/10/29','1','pay','afsfah dgsg','A04CF7F2','IIITHYD','ICON08','1500','joseph.anand@gmail.com');
INSERT INTO requestvalues VALUES ('icon4','2008/10/30','1','pay','ghj gj','A04CF7F2','IIITHYD','ICON08','3000','test@iiit.ac.in');
INSERT INTO requestvalues VALUES ('icon5','2008/11/07','1','pay','Monojit Choudhury','A04CF7F2','IIITHYD','ICON08','6000','monojitc@microsoft.com');
INSERT INTO requestvalues VALUES ('icon6','2008/11/09','1','pay','Monojit Choudhury','A04CF7F2','IIITHYD','ICON08','6000','monojitc@microsoft.com');
INSERT INTO requestvalues VALUES ('icon7','2008/11/09','1','pay','Ramanand Janardhanan','A04CF7F2','IIITHYD','ICON08','6000','ramanand@it.iitb.ac.in');
INSERT INTO requestvalues VALUES ('icon8','2008/11/11','1','pay','Plaban Bhowmick','A04CF7F2','IIITHYD','ICON08','1500','');
INSERT INTO requestvalues VALUES ('icon9','2008/11/12','1','pay','Doren Singh Thoudam','A04CF7F2','IIITHYD','ICON08','3750','thoudam.doren@gmail.com');
INSERT INTO requestvalues VALUES ('icon13','2008/11/12','1','pay','SHIVA KUMAR H R','A04CF7F2','IIITHYD','ICON08','1500','shivahr@ee.iisc.ernet.in');
INSERT INTO requestvalues VALUES ('icon11','2008/11/12','1','pay','Rohit Shrestha','A04CF7F2','IIITHYD','ICON08','3750','rohitshrestha07@gmail.com');
INSERT INTO requestvalues VALUES ('icon12','2008/11/12','1','pay','gdfg gf','A04CF7F2','IIITHYD','ICON08','400','sdfsd@dfg.com');
INSERT INTO requestvalues VALUES ('icon12','2008/11/12','1','pay','gdfg gf','A04CF7F2','IIITHYD','ICON08','400','sdfsd@dfg.com');
INSERT INTO requestvalues VALUES ('icon14','2008/11/13','1','pay','Sanjay Chatterji','A04CF7F2','IIITHYD','ICON08','1500','sanjaychatter@gmail.com');
INSERT INTO requestvalues VALUES ('icon15','2008/11/13','1','pay','Sanjay Chatterji','A04CF7F2','IIITHYD','ICON08','1500','sanjaychatter@yahoo.com');
INSERT INTO requestvalues VALUES ('icon16','2008/11/13','1','pay','Biswanath Barik','A04CF7F2','IIITHYD','ICON08','1500','b_barik@rediffmail.com');
INSERT INTO requestvalues VALUES ('icon17','2008/11/13','1','pay','Sanjay Chatterji','A04CF7F2','IIITHYD','ICON08','1500','sanjay_chatter@yahoo.com');
INSERT INTO requestvalues VALUES ('icon18','2008/11/13','1','pay','Kalika Bali','A04CF7F2','IIITHYD','ICON08','6000','kalikab@microsoft.com');
INSERT INTO requestvalues VALUES ('icon19','2008/11/13','1','pay','Rabin Mondal','A04CF7F2','IIITHYD','ICON08','3750','zand@gh.com');
INSERT INTO requestvalues VALUES ('icon20','2008/11/14','1','pay','Sujan Kr Saha','A04CF7F2','IIITHYD','ICON08','1500','sujan.kr.saha@gmail.com');
INSERT INTO requestvalues VALUES ('icon21','2008/11/14','1','pay','PARTHA PAKRAY','A04CF7F2','IIITHYD','ICON08','3750','');
INSERT INTO requestvalues VALUES ('icon21','2008/11/14','1','pay','PARTHA PAKRAY','A04CF7F2','IIITHYD','ICON08','3750','');
INSERT INTO requestvalues VALUES ('icon23','2008/11/14','1','pay','PARTHA PAKRAY','A04CF7F2','IIITHYD','ICON08','3750','');
INSERT INTO requestvalues VALUES ('icon24','2008/11/14','1','pay','PARTHA PAKRAY','A04CF7F2','IIITHYD','ICON08','3750','');
INSERT INTO requestvalues VALUES ('icon25','2008/11/14','1','pay','Dipankar Das','A04CF7F2','IIITHYD','ICON08','1900','');
INSERT INTO requestvalues VALUES ('icon26','2008/11/14','1','pay','SIVAJI BANDYOPADHYAY','A04CF7F2','IIITHYD','ICON08','3000','sivaji_cse_ju@yahoo.com');
INSERT INTO requestvalues VALUES ('icon27','2008/11/14','1','pay','S S S Kumar CHIVUKULA','A04CF7F2','IIITHYD','ICON08','6000','csameerkumar@firstam.com');
INSERT INTO requestvalues VALUES ('icon28','2008/11/14','1','pay','Asif Ekbal','A04CF7F2','IIITHYD','ICON08','1500','asif.ekbal@gmail.com');
INSERT INTO requestvalues VALUES ('icon29','2008/11/14','1','pay','Asif Ekbal','A04CF7F2','IIITHYD','ICON08','1500','asif.ekbal@gmail.com');
INSERT INTO requestvalues VALUES ('icon30','2008/11/14','1','pay','Asif Ekbal','A04CF7F2','IIITHYD','ICON08','1500','asif.ekbal@gmail.com');
INSERT INTO requestvalues VALUES ('icon31','2008/11/14','1','pay','Santanu Pal','A04CF7F2','IIITHYD','ICON08','3750','santanupersonal1@gmail.com');
INSERT INTO requestvalues VALUES ('icon32','2008/11/14','1','pay','Santanu Pal','A04CF7F2','IIITHYD','ICON08','3750','santanupersonal1@gmail.com');
INSERT INTO requestvalues VALUES ('icon33','2008/11/14','1','pay','Santanu Pal','A04CF7F2','IIITHYD','ICON08','3750','santanupersonal1@gmail.com');
INSERT INTO requestvalues VALUES ('icon33','2008/11/14','1','pay','Dipankar Das','A04CF7F2','IIITHYD','ICON08','1900','');
INSERT INTO requestvalues VALUES ('icon33','2008/11/14','1','pay','Santanu Pal','A04CF7F2','IIITHYD','ICON08','3750','santanupersonal1@gmail.com');
INSERT INTO requestvalues VALUES ('icon36','2008/11/14','1','pay','Dipankar Das','A04CF7F2','IIITHYD','ICON08','1900','');
INSERT INTO requestvalues VALUES ('icon37','2008/11/14','1','pay','Dipankar Das','A04CF7F2','IIITHYD','ICON08','1900','');
INSERT INTO requestvalues VALUES ('icon37','2008/11/14','1','pay','Dipankar Das','A04CF7F2','IIITHYD','ICON08','1900','');
INSERT INTO requestvalues VALUES ('icon39','2008/11/14','1','pay','Ramanand Janardhanan','A04CF7F2','IIITHYD','ICON08','6000','ramanand@it.iitb.ac.in');
INSERT INTO requestvalues VALUES ('icon40','2008/11/15','1','pay','A. Nayeemulla Khan','A04CF7F2','IIITHYD','ICON08','6000','an_k3@rediffmail.com');
INSERT INTO requestvalues VALUES ('icon41','2008/11/15','1','pay','A. Nayeemulla Khan','A04CF7F2','IIITHYD','ICON08','6000','an_k3@rediffmail.com');
INSERT INTO requestvalues VALUES ('icon42','2008/11/15','1','pay','A. Nayeemulla Khan','A04CF7F2','IIITHYD','ICON08','6000','an_k3@yahoo.com');
INSERT INTO requestvalues VALUES ('icon43','2008/11/15','1','pay','A.Nayeemulla Khan','A04CF7F2','IIITHYD','ICON08','6000','shahina_iitm@rediffmail.com');
INSERT INTO requestvalues VALUES ('icon44','2008/11/16','1','pay','sitanath biswas','A04CF7F2','IIITHYD','ICON08','3750','sitanath_biswas2006@yahoo.com');
INSERT INTO requestvalues VALUES ('icon45','2008/11/16','1','pay','sitanath biswas','A04CF7F2','IIITHYD','ICON08','3750','sitanath_biswas2006@yahoo.com');
INSERT INTO requestvalues VALUES ('icon46','2008/11/19','1','pay','Cohan Carlos','A04CF7F2','IIITHYD','ICON08','7200','cohan.sujay@gmail.com');
INSERT INTO requestvalues VALUES ('icon47','2008/11/19','1','pay','Cohan Carlos','A04CF7F2','IIITHYD','ICON08','7200','cohan.sujay@gmail.com');
INSERT INTO requestvalues VALUES ('icon48','2008/11/22','1','pay','Shivapratap Gopakumar','A04CF7F2','IIITHYD','ICON08','4500','shivapratap@gmail.com');
INSERT INTO requestvalues VALUES ('icon49','2008/11/22','1','pay','Shivapratap Gopakumar','A04CF7F2','IIITHYD','ICON08','4500','g_shivapratap@ettimadai.amrita.edu');
INSERT INTO requestvalues VALUES ('icon50','2008/11/22','1','pay','Dhanalakshmi V','A04CF7F2','IIITHYD','ICON08','900','v_dhanalakshmi@ettimadai.amrita.edu');
INSERT INTO requestvalues VALUES ('icon51','2008/11/22','1','pay','VIjaya Vijayakumar','A04CF7F2','IIITHYD','ICON08','900','ms_vijaya@ettimadai.amrita.edu');
INSERT INTO requestvalues VALUES ('icon52','2008/11/25','1','pay','A Kumaran','A04CF7F2','IIITHYD','ICON08','7200','a.kumaran@microsoft.com');
INSERT INTO requestvalues VALUES ('icon53','2008/11/25','1','pay','Dandapat Sandipan','A04CF7F2','IIITHYD','ICON08','7200','');
INSERT INTO requestvalues VALUES ('icon54','2008/11/25','1','pay','A Kumaran','A04CF7F2','IIITHYD','ICON08','7200','a.kumaran@microsoft.com');
INSERT INTO requestvalues VALUES ('icon55','2008/11/25','1','pay','Dhirendra Singh','A04CF7F2','IIITHYD','ICON08','2300','tunmun007@gmail.com');
INSERT INTO requestvalues VALUES ('icon56','2008/11/26','1','pay','A Kumaran','A04CF7F2','IIITHYD','ICON08','7200','a.kumaran@microsoft.com');
INSERT INTO requestvalues VALUES ('icon57','2008/11/26','1','pay','Abhijit Payeng','A04CF7F2','IIITHYD','ICON08','1800','apayeng@gmail.com');
INSERT INTO requestvalues VALUES ('icon58','2008/11/26','1','pay','Janardan Mishra','A04CF7F2','IIITHYD','ICON08','2300','j.misra01@gmail.com');
INSERT INTO requestvalues VALUES ('icon58','2008/11/26','1','pay','Janardan Mishra','A04CF7F2','IIITHYD','ICON08','2300','j.misra01@gmail.com');
INSERT INTO requestvalues VALUES ('icon58','2008/11/26','1','pay','Janardan Mishra','A04CF7F2','IIITHYD','ICON08','2300','j.misra01@gmail.com');
INSERT INTO requestvalues VALUES ('icon61','2008/11/26','1','pay','SARAVANAN KRISHNAN','A04CF7F2','IIITHYD','ICON08','7200','');
INSERT INTO requestvalues VALUES ('icon62','2008/11/26','1','pay','Abhijit Payeng','A04CF7F2','IIITHYD','ICON08','500','abhijit_payeng@yahoo.co.in');
INSERT INTO requestvalues VALUES ('icon63','2008/11/26','1','pay','Abhijit Payeng','A04CF7F2','IIITHYD','ICON08','500','abhijit_payeng@yahoo.co.in');
INSERT INTO requestvalues VALUES ('icon64','2008/11/27','1','pay','Tirthankar Dasgupta','A04CF7F2','IIITHYD','ICON08','1800','iamtirthankar@gmail.com');
INSERT INTO requestvalues VALUES ('icon65','2008/11/27','1','pay','Tirthankar Dasgupta','A04CF7F2','IIITHYD','ICON08','1800','iamtirthankar@gmail.com');
INSERT INTO requestvalues VALUES ('icon66','2008/11/27','1','pay','A Kumaran','A04CF7F2','IIITHYD','ICON08','7200','a.kumaran@microsoft.com');
INSERT INTO requestvalues VALUES ('icon67','2008/11/29','1','pay','ss ss','A04CF7F2','IIITHYD','ICON08','1800','sss@dkjfdkj.com');
INSERT INTO requestvalues VALUES ('icon68','2008/12/02','1','pay','saurabh kumar','A04CF7F2','IIITHYD','ICON08','1500','kunnu10@gmail.com');
INSERT INTO requestvalues VALUES ('icon69','2008/12/02','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','altaf.mahmud@gmail.com');
INSERT INTO requestvalues VALUES ('icon70','2008/12/02','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','altaf.mahmud@gmail.com');
INSERT INTO requestvalues VALUES ('icon71','2008/12/02','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','raptor167@gmail.com');
INSERT INTO requestvalues VALUES ('icon72','2008/12/02','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','altaf.mahmud@gmail.com');
INSERT INTO requestvalues VALUES ('icon73','2008/12/02','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','altaf.mahmud@gmail.com');
INSERT INTO requestvalues VALUES ('icon74','2008/12/03','1','pay','Janardan Mishra','A04CF7F2','IIITHYD','ICON08','2300','j.misra01@gmail.com');
INSERT INTO requestvalues VALUES ('icon75','2008/12/03','1','pay','Milind Patil','A04CF7F2','IIITHYD','ICON08','3600','');
INSERT INTO requestvalues VALUES ('icon76','2008/12/03','1','pay','Milind Patil','A04CF7F2','IIITHYD','ICON08','2300','kumod15@gmail.com');
INSERT INTO requestvalues VALUES ('icon76','2008/12/03','1','pay','Janardan Mishra','A04CF7F2','IIITHYD','ICON08','2300','j.misra01@gmail.com');
INSERT INTO requestvalues VALUES ('icon78','2008/12/03','1','pay','Rahul Mhaiskar','A04CF7F2','IIITHYD','ICON08','2300','badshah_74@rediffmail.com');
INSERT INTO requestvalues VALUES ('icon79','2008/12/03','1','pay','subodh kumar','A04CF7F2','IIITHYD','ICON08','2300','tosubodhkumar@gmail.com');
INSERT INTO requestvalues VALUES ('icon80','2008/12/05','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','altaf.mahmud@gmail.com');
INSERT INTO requestvalues VALUES ('icon81','2008/12/05','1','pay','Janardan Mishra','A04CF7F2','IIITHYD','ICON08','2300','j.misra01@gmail.com');
INSERT INTO requestvalues VALUES ('icon82','2008/12/05','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','altaf.mahmud@gmail.com');
INSERT INTO requestvalues VALUES ('icon83','2008/12/06','1','pay','Vaishali Patil','A04CF7F2','IIITHYD','ICON08','4500','vaishali.imrd@gmail.com');
INSERT INTO requestvalues VALUES ('icon84','2008/12/06','1','pay','Altaf Mahmud','A04CF7F2','IIITHYD','ICON08','9000','altaf.mahmud@gmail.com');
INSERT INTO requestvalues VALUES ('icon85','2008/12/09','1','pay','Anusha Jain','A04CF7F2','IIITHYD','ICON08','10800','anusha.jain@nechclst.in');
INSERT INTO requestvalues VALUES ('icon86','2008/12/09','1','pay','Parul Nath','A04CF7F2','IIITHYD','ICON08','10800','parul.nath@nechclst.in');
INSERT INTO requestvalues VALUES ('icon87','2008/12/09','1','pay','Anusha Jain','A04CF7F2','IIITHYD','ICON08','10800','anusha.jain@nechclst.in');
INSERT INTO requestvalues VALUES ('icon88','2008/12/09','1','pay','Pavel Stranak','A04CF7F2','IIITHYD','ICON08','13500','stranak@ufal.mff.cuni.cz');
INSERT INTO requestvalues VALUES ('icon89','2008/12/09','1','pay','Pavel Stranak','A04CF7F2','IIITHYD','ICON08','13500','pavel.stranak@mff.cuni.cz');
INSERT INTO requestvalues VALUES ('icon90','2008/12/10','1','pay','Dhirendra Singh','A04CF7F2','IIITHYD','ICON08','2300','tunmun007@gmail.com');
INSERT INTO requestvalues VALUES ('icon91','2008/12/10','1','pay','satyendra kumar','A04CF7F2','IIITHYD','ICON08','2300','meetsatyendrakumar@gmail.com');
INSERT INTO requestvalues VALUES ('icon92','2008/12/10','1','pay','Anjani Ray','A04CF7F2','IIITHYD','ICON08','900','');
INSERT INTO requestvalues VALUES ('icon93','2008/12/10','1','pay','Leonardo Lesmo','A04CF7F2','IIITHYD','ICON08','9000','lesmo@di.unito.it');
INSERT INTO requestvalues VALUES ('icon94','2008/12/19','1','pay','Anusha Jain','A04CF7F2','IIITHYD','ICON08','10800','anusha.jain@nechclst.in');
INSERT INTO requestvalues VALUES ('icon95','2008/12/24','1','pay','Anusha Jain','A04CF7F2','IIITHYD','ICON08','10800','anusha.jain@nechclst.in');

--
-- Table structure for table `responsevalues`
--

CREATE TABLE responsevalues (
  transactionID varchar(15) default NULL,
  vpc_trans_response_code varchar(5) default NULL,
  trans_response_code_desc varchar(20) default NULL,
  message varchar(50) default NULL,
  Receipt_number varchar(20) default NULL,
  Transaction_number varchar(20) default NULL,
  Acquirer_Response_Code varchar(20) default NULL,
  Bank_authorizationID varchar(20) default NULL,
  Batch_number varchar(20) default NULL,
  Card_type varchar(10) default NULL
) TYPE=MyISAM;

--
-- Dumping data for table `responsevalues`
--


INSERT INTO responsevalues VALUES ('','No Va','Unable to be determi','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value R');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','831818000510','138','00','008291','20081113','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','831818000510','138','00','008291','20081113','MC');
INSERT INTO responsevalues VALUES ('icon20','0','Transaction Successf','Approved','831917000514','142','00','084157','20081114','VC');
INSERT INTO responsevalues VALUES ('icon24','0','Transaction Successf','Approved','831919000515','143','00','093987','20081114','MC');
INSERT INTO responsevalues VALUES ('icon26','0','Transaction Successf','Approved','831920000516','144','00','057328','20081114','VC');
INSERT INTO responsevalues VALUES ('icon27','0','Transaction Successf','Approved','831921000517','145','00','062662','20081114','MC');
INSERT INTO responsevalues VALUES ('icon28','F','3D Secure Authentica','The card holder was not authorised. This is used i','No Value Returned','0','No Value Returned','No Value Returned','0','No Value R');
INSERT INTO responsevalues VALUES ('icon30','0','Transaction Successf','Approved','831922000518','146','00','017157','20081114','VC');
INSERT INTO responsevalues VALUES ('icon33','0','Transaction Successf','Approved','831923000519','147','00','024746','20081114','MC');
INSERT INTO responsevalues VALUES ('icon33','0','Transaction Successf','Approved','831923000519','147','00','024746','20081114','MC');
INSERT INTO responsevalues VALUES ('icon37','0','Transaction Successf','Approved','831923000520','148','00','026091','20081114','MC');
INSERT INTO responsevalues VALUES ('icon37','0','Transaction Successf','Approved','831923000520','148','00','026091','20081114','MC');
INSERT INTO responsevalues VALUES ('icon39','0','Transaction Successf','Approved','832001000521','149','00','462253','20081114','VC');
INSERT INTO responsevalues VALUES ('icon43','0','Transaction Successf','Approved','832014000522','150','00','425157','20081115','MC');
INSERT INTO responsevalues VALUES ('icon44','4','Expired Card','Expired Card','832203000523','151','54','No Value Returned','20081116','VC');
INSERT INTO responsevalues VALUES ('icon45','0','Transaction Successf','Approved','832203000524','152','00','098731','20081116','VC');
INSERT INTO responsevalues VALUES ('icon45','0','Transaction Successf','Approved','832203000524','152','00','098731','20081116','VC');
INSERT INTO responsevalues VALUES ('icon46','5','Insufficient funds','Insufficient Funds','832414000526','154','51','No Value Returned','20081119','MC');
INSERT INTO responsevalues VALUES ('icon47','0','Transaction Successf','Approved','832414000527','155','00','555033','20081119','VC');
INSERT INTO responsevalues VALUES ('icon48','2','Bank Declined Transa','Declined','832716000531','159','05','No Value Returned','20081122','VC');
INSERT INTO responsevalues VALUES ('icon48','2','Bank Declined Transa','Declined','832716000531','159','05','No Value Returned','20081122','VC');
INSERT INTO responsevalues VALUES ('icon49','0','Transaction Successf','Approved','832717000532','160','00','010193','20081122','VC');
INSERT INTO responsevalues VALUES ('icon50','0','Transaction Successf','Approved','832717000533','161','00','094182','20081122','VC');
INSERT INTO responsevalues VALUES ('icon51','0','Transaction Successf','Approved','832717000534','162','00','011172','20081122','VC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','832820000535','163','00','992660','20081123','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','832820000536','164','00','962432','20081123','MC');
INSERT INTO responsevalues VALUES ('icon53','0','Transaction Successf','Approved','833017000537','165','00','025258','20081125','VC');
INSERT INTO responsevalues VALUES ('icon54','2','Bank Declined Transa','Declined','833021000538','166','05','No Value Returned','20081125','VC');
INSERT INTO responsevalues VALUES ('icon56','2','Bank Declined Transa','Declined','833116000539','167','05','No Value Returned','20081126','VC');
INSERT INTO responsevalues VALUES ('icon57','1','Unknown Error','Unspecified Failure','833118000540','168','57','No Value Returned','20081126','VC');
INSERT INTO responsevalues VALUES ('icon57','1','Unknown Error','Unspecified Failure','833118000540','168','57','No Value Returned','20081126','VC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','833120000541','169','00','971381','20081126','VC');
INSERT INTO responsevalues VALUES ('icon58','2','Bank Declined Transa','Declined','833121000542','170','14','No Value Returned','20081126','MC');
INSERT INTO responsevalues VALUES ('icon61','0','Transaction Successf','Approved','833122000543','171','00','094739','20081126','VC');
INSERT INTO responsevalues VALUES ('icon62','1','Unknown Error','Unspecified Failure','833202000544','172','57','No Value Returned','20081126','VC');
INSERT INTO responsevalues VALUES ('icon63','1','Unknown Error','Unspecified Failure','833202000545','173','57','No Value Returned','20081126','VC');
INSERT INTO responsevalues VALUES ('icon65','0','Transaction Successf','Approved','833216000546','174','00','029008','20081127','VC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','833216000546','174','00','029008','20081127','VC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','833216000546','174','00','029008','20081127','VC');
INSERT INTO responsevalues VALUES ('icon66','0','Transaction Successf','Approved','833221000547','175','00','094591','20081127','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','833221000547','175','00','094591','20081127','MC');
INSERT INTO responsevalues VALUES ('','No Va','Unable to be determi','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value R');
INSERT INTO responsevalues VALUES ('icon74','2','Bank Declined Transa','Declined','833820000552','180','14','No Value Returned','20081203','MC');
INSERT INTO responsevalues VALUES ('icon76','2','Bank Declined Transa','Declined','833821000553','181','14','No Value Returned','20081203','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','833918000555','183','00','T42953','20081204','MC');
INSERT INTO responsevalues VALUES ('icon80','0','Transaction Successf','Approved','834008000556','184','00','16154Z','20081205','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','833216000546','174','00','029008','20081127','VC');
INSERT INTO responsevalues VALUES ('icon81','4','Expired Card','Expired Card','834019000559','187','54','No Value Returned','20081205','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','834115000568','196','00','001850','20081206','VC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','834120000569','197','00','063577','20081206','VC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','834214000573','201','00','989516','20081207','VC');
INSERT INTO responsevalues VALUES ('','No Va','Unable to be determi','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value Returned','No Value R');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','834321000580','208','00','075623','20081208','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','834323000581','209','00','087763','20081208','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','834323000582','210','00','028842','20081208','MC');
INSERT INTO responsevalues VALUES ('icon86','0','Transaction Successf','Approved','834420000590','218','00','817617','20081209','MC');
INSERT INTO responsevalues VALUES ('icon87','0','Transaction Successf','Approved','834420000591','219','00','722808','20081209','MC');
INSERT INTO responsevalues VALUES ('icon93','0','Transaction Successf','Approved','834600000604','233','00','051186','20081210','MC');
INSERT INTO responsevalues VALUES ('','0','Transaction Successf','Approved','833216000546','174','00','029008','20081127','VC');

--
-- Table structure for table `transID`
--

CREATE TABLE transID (
  trans_no int(10) NOT NULL auto_increment,
  vpc_url varchar(50) default NULL,
  locale varchar(15) default NULL,
  ticketno varchar(20) default NULL,
  trans_source_subtype varchar(30) default NULL,
  PRIMARY KEY  (trans_no)
) TYPE=MyISAM;

--
-- Dumping data for table `transID`
--


INSERT INTO transID VALUES (1,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (2,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (3,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (4,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (5,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (6,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (7,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (8,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (9,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (10,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (11,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (12,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (13,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (14,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (15,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (16,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (17,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (18,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (19,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (20,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (21,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (22,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (23,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (24,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (25,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (26,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (27,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (28,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (29,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (30,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (31,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (32,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (33,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (34,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (35,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (36,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (37,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (38,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (39,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (40,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (41,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (42,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (43,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (44,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (45,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (46,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (47,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (48,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (49,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (50,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (51,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (52,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (53,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (54,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (55,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (56,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (57,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (58,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (59,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (60,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (61,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (62,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (63,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (64,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (65,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (66,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (67,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (68,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (69,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (70,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (71,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (72,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (73,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (74,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (75,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (76,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (77,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (78,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (79,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (80,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (81,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (82,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (83,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (84,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (85,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (86,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (87,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (88,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (89,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (90,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (91,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (92,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (93,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (94,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (95,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');
INSERT INTO transID VALUES (96,'https://migs.mastercard.com.au/vpcpay?Title=PHP+VP','en','','');

