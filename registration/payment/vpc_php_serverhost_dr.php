<?php
// Initialisation
// ==============

$txt = json_encode($_POST);
$myfile = file_put_contents('logstxninfo.txt', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);


function sendMyConfimationMailPlease($to, $subject, $mailBody)
{
    require_once 'mailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    // $mail->SMTPDebug = 3;                              // Enable verbose debug output

     $mail->isSMTP();                                      // Set mailer to use SMTP
     $mail->Host = 'mail.iiit.ac.in';                   // Specify main and backup SMTP servers
     $mail->SMTPAuth = true;                               // Enable SMTP authentication
     $mail->SMTPAuth = false;                               // Enable SMTP authentication
    // $mail->Username = 'moodle@iiit.ac.in';                 // SMTP username
    // $mail->Password = 'password';                           // SMTP password
     $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
     $mail->SMTPAutoTLS = false;
     $mail->Port = 25;                                 // TCP port to connect to

    $mail->setFrom('smm18@iiit.ac.in', 'SMMW 2018 Registrations');
    $mail->addAddress($to);     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body    = str_replace("\n", "<br>\n", $mailBody);
    $mail->AltBody = str_replace("\n", "\r\n", $mailBody);
    $mail->send();
}

//
include('VPCPaymentConnection.php');
$conn = new VPCPaymentConnection();


// This is secret for encoding the SHA256 hash
// This secret will vary from merchant to merchant

$secureSecret = "76AA74EAC9CB560CF02BA8DB621BDA33";

// Set the Secure Hash Secret used by the VPC connection object
$conn->setSecureSecret($secureSecret);


// Set the error flag to false
$errorExists = false;



// *******************************************
// START OF MAIN PROGRAM
// *******************************************


// This is the title for display
$title  = $_GET["Title"];


// Add VPC post data to the Digital Order
foreach($_GET as $key => $value) {
    if (($key!="vpc_SecureHash") && ($key != "vpc_SecureHashType") && ((substr($key, 0,4)=="vpc_") || (substr($key,0,5) =="user_"))) {
        $conn->addDigitalOrderField($key, $value);
    }
}

// Obtain a one-way hash of the Digital Order data and
// check this against what was received.
$serverSecureHash	= array_key_exists("vpc_SecureHash", $_GET)	? $_GET["vpc_SecureHash"] : "";
$secureHash = $conn->hashAllFields();
if ($secureHash==$serverSecureHash) {
    $hashValidated = "<font color='#00AA00'><strong>CORRECT</strong></font>";
} else {
    $hashValidated = "<font color='#FF0066'><strong>INVALID HASH</strong></font>";
    $errorsExist = true;
}




/*  If there has been a merchant secret set then sort and loop through all the
    data in the Virtual Payment Client response. while we have the data, we can
    append all the fields that contain values (except the secure hash) so that
    we can create a hash and validate it against the secure hash in the Virtual
    Payment Client response.

    NOTE: If the vpc_TxnResponseCode in not a single character then
    there was a Virtual Payment Client error and we cannot accurately validate
    the incoming data from the secure hash.

    // remove the vpc_TxnResponseCode code from the response fields as we do not
    // want to include this field in the hash calculation

    if (secureSecret != null && secureSecret.length() > 0 &&
        (fields.get("vpc_TxnResponseCode") != null || fields.get("vpc_TxnResponseCode") != "No Value Returned")) {

        // create secure hash and append it to the hash map if it was created
        // remember if secureSecret = "" it wil not be created
        String secureHash = vpc3conn.hashAllFields(fields);

        // Validate the Secure Hash (remember  hashes are not case sensitive)
        if (vpc_Txn_Secure_Hash.equalsIgnoreCase(secureHash)) {
            // Secure Hash validation succeeded, add a data field to be
            // displayed later.
            hashValidated = "<font color='#00AA00'><strong>CORRECT</strong></font>";
        } else {
            // Secure Hash validation failed, add a data field to be
            // displayed later.
            errorExists = true;
            hashValidated = "<font color='#FF0066'><strong>INVALID HASH</strong></font>";
        }
    } else {
        // Secure Hash was not validated,
        hashValidated = "<font color='orange'><strong>Not Calculated - No 'SECURE_SECRET' present.</strong></font>";
    }
*/

    // Extract the available receipt fields from the VPC Response
    // If not present then let the value be equal to 'Unknown'
    // Standard Receipt Data


$Title 				= array_key_exists("Title", $_GET) 						? $_GET["Title"] 				: "";
$againLink 			= array_key_exists("AgainLink", $_GET) 					? $_GET["AgainLink"] 			: "";
$amount 			= array_key_exists("vpc_Amount", $_GET) 				? $_GET["vpc_Amount"] 			: "";
$locale 			= array_key_exists("vpc_Locale", $_GET) 				? $_GET["vpc_Locale"] 			: "";
$batchNo 			= array_key_exists("vpc_BatchNo", $_GET) 				? $_GET["vpc_BatchNo"] 			: "";
$command 			= array_key_exists("vpc_Command", $_GET) 				? $_GET["vpc_Command"] 			: "";
$message 			= array_key_exists("vpc_Message", $_GET) 				? $_GET["vpc_Message"]			: "";
$version  			= array_key_exists("vpc_Version", $_GET) 				? $_GET["vpc_Version"] 			: "";
$cardType   		= array_key_exists("vpc_Card", $_GET) 					? $_GET["vpc_Card"] 			: "";
$orderInfo 			= array_key_exists("vpc_OrderInfo", $_GET) 				? $_GET["vpc_OrderInfo"] 		: "";
$receiptNo 			= array_key_exists("vpc_ReceiptNo", $_GET) 				? $_GET["vpc_ReceiptNo"] 		: "";
$merchantID  		= array_key_exists("vpc_Merchant", $_GET) 				? $_GET["vpc_Merchant"] 		: "";
$merchTxnRef 		= array_key_exists("vpc_MerchTxnRef", $_GET) 			? $_GET["vpc_MerchTxnRef"]		: "";
$authorizeID 		= array_key_exists("vpc_AuthorizeId", $_GET) 			? $_GET["vpc_AuthorizeId"] 		: "";
$transactionNo  	= array_key_exists("vpc_TransactionNo", $_GET) 			? $_GET["vpc_TransactionNo"] 	: "";
$acqResponseCode 	= array_key_exists("vpc_AcqResponseCode", $_GET) 		? $_GET["vpc_AcqResponseCode"] 	: "";
$txnResponseCode 	= array_key_exists("vpc_TxnResponseCode", $_GET) 		? $_GET["vpc_TxnResponseCode"] 	: "";
$riskOverallResult	= array_key_exists("vpc_RiskOverallResult", $_GET) 		? $_GET["vpc_RiskOverallResult"]: "";

		// Obtain the 3DS response
$vpc_3DSECI				= array_key_exists("vpc_3DSECI", $_GET) 			? $_GET["vpc_3DSECI"] : "";
$vpc_3DSXID				= array_key_exists("vpc_3DSXID", $_GET) 			? $_GET["vpc_3DSXID"] : "";
$vpc_3DSenrolled 		= array_key_exists("vpc_3DSenrolled", $_GET) 		? $_GET["vpc_3DSenrolled"] : "";
$vpc_3DSstatus 			= array_key_exists("vpc_3DSstatus", $_GET) 			? $_GET["vpc_3DSstatus"] : "";
$vpc_VerToken 			= array_key_exists("vpc_VerToken", $_GET) 			? $_GET["vpc_VerToken"] : "";
$vpc_VerType 			= array_key_exists("vpc_VerType", $_GET) 			? $_GET["vpc_VerType"] : "";
$vpc_VerStatus			= array_key_exists("vpc_VerStatus", $_GET) 			? $_GET["vpc_VerStatus"] : "";
$vpc_VerSecurityLevel	= array_key_exists("vpc_VerSecurityLevel", $_GET) 	? $_GET["vpc_VerSecurityLevel"] : "";


    // CSC Receipt Data
$cscResultCode 	= array_key_exists("vpc_CSCResultCode", $_GET)  			? $_GET["vpc_CSCResultCode"] : "";
$ACQCSCRespCode = array_key_exists("vpc_AcqCSCRespCode", $_GET) 			? $_GET["vpc_AcqCSCRespCode"] : "";

// Get the descriptions behind the QSI, CSC and AVS Response Codes
    // Only get the descriptions if the string returned is not equal to "No Value Returned".

$txnResponseCodeDesc = "";
$cscResultCodeDesc = "";
$avsResultCodeDesc = "";

    if ($txnResponseCode != "No Value Returned") {
        $txnResponseCodeDesc = getResultDescription($txnResponseCode);
    }

    if ($cscResultCode != "No Value Returned") {
        $cscResultCodeDesc = getCSCResultDescription($cscResultCode);
    }


		$error = "";
    // Show this page as an error page if error condition
    if ($txnResponseCode=="7" || $txnResponseCode=="No Value Returned" || $errorExists) {
        $error = "Error ";
    }

    // FINISH TRANSACTION - Process the VPC Response Data
    // =====================================================
    // For the purposes of demonstration, we simply display the Result fields on a
    // web page.


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>Response Page</title>
        <meta http-equiv="Content-Type" content="text/html, charset=iso-8859-1">
        <style type='text/css'>
            input {
                font-family: Verdana, Arial, sans-serif;
                color: #08185A;
                border: 2px solid #003156;
                padding: .5em;
                background-color: #E1E1E1;
                font-weight: bold;
            }

            input[type=submit] {
                cursor: pointer;
            }

            body {
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                margin: 0;
                padding: 0;
            }

            #pagetitle {
                background-color: #003156;
                color: #FFFFFF;
                font-size: 1.4em;
                padding: .4em;
                text-align: center;
                font-family: serif;
            }

            .container {
                max-width: 1080px;
                margin: auto;
                padding: 1em;
            }

            table {
                margin: auto;
            }

            td {
                padding: 10px;
                width: 50%;
            }
        </style>
    </head>
    <body>
<div id="pagetitle">
	SMMW 2018
    <br/>
    <small style="font-size: 0.5em;">IIIT-H, Hyderabad, India</small>
</div>
    <center><h1> Payment - Response Page</h1></center>
        <table width="85%" align="center" cellpadding="5" border="0">
            <tr>
                <td align="right"><strong><i>Merchant Transaction Reference: </i></strong></td>
                <td><?php echo $merchTxnRef; ?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Purchase Amount: </i></strong></td>
                <td><?php echo $amount / 100; ?></td>
            </tr>

<!-- To store details in responsevalues table-->
<?php
$connection = mysql_connect("localhost", "smmwuser", "YjUzNmJl");
mysql_select_db("smmw2018", $connection) or die(mysql_error());

//To create unique transaction ID
session_start();

$transid=$_SESSION['uniquetransid'];

$resdesc=$txnResponseCode;  
$amount2 = $amount/100;
$query=(mysql_query("insert into responsevalues values('$transid','$txnResponseCode','$resdesc','$message','$receiptNo','$transactionNo','$acqResponseCode','$authorizeID','$batchNo','$cardType', '$amount2')"));

$to=$_SESSION['email'];
$from="smm18@iiit.ac.in";
$subject = "SMMW-2018: Payment Details.";
$headers = "From: $from";
$msg = " Dear ".$_SESSION['payeename'];
$msg .=" 
	 
               SMMW 2018 payment details are as follows
	    
	  1. Reciept Number: $receiptNo
          2. Card Type : $cardType
          3. Transaction Number : $transactionNo
          4. Message : $message
          5. Amount : Rs. $amount2
	  
	  For further queries please contact
	  
	       SMMW-2018
	       E-mail: smm18@iiit.ac.in";
  sendMyConfimationMailPlease($to, $subject, $msg);
  sendMyConfimationMailPlease('smm18@iiit.ac.in', $subject, $msg);
?>

 
             
            <tr>
                <td align="right"><strong><i>Transaction Response Code Description: </i></strong></td>
                <td><?=$txnResponseCode?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Message: </i></strong></td>
                <td><?php echo $message; ?></td>
            </tr>
<?php
    // only display the following fields if not an error condition
    if ($txnResponseCode != "7" && $txnResponseCode != "No Value Returned") { 
?>
            <tr>
                <td align="right"><strong><i>Receipt Number: </i></strong></td>
                <td><?php echo $receiptNo; ?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Transaction Number: </i></strong></td>
                <td><?php echo $transactionNo; ?></td>
            </tr>
        <?php
        /*
            <tr class="shade">
                <td align="right"><strong><i>Bank Authorization ID: </i></strong></td>
                <td><?php echo $authorizeID; ?></td>
            </tr>

            <tr>
                <td align="right"><strong><i>Batch Number: </i></strong></td>
                <td><?php echo $batchNo; ?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Card Type: </i></strong></td>
                <td><?php echo $cardType;?></td>
            </tr>

            <tr>
                <td colspan="2" align="center">
                    <font color="#C1C1C1">Fields above are for a Standard Transaction<br />
                    <HR />
                    Fields below are additional fields for extra functionality.</font><br />
                </td>
            </tr>
            <tr>
                <td colspan="2"><HR /></td>
            </tr>
            <tr class="title">
                <td colspan="2" height="25"><P><strong>&nbsp;3-D Secure Fields</strong></P></td>
            </tr>
            <tr>
                <td align="right"><strong><i>Unique 3DS transaction identifier: </i></strong></td>
                <td class="red"><?=$xid?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>3DS Authentication Verification Value: </i></strong></td>
                <td class="red"><?=$token?></td>
            </tr>
            <tr>
                <td align="right"><strong><i>3DS Electronic Commerce Indicator: </i></strong></td>
                <td class="red"><?=$acqECI?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>3DS Authentication Scheme: </i></strong></td>
                <td class="red"><?=$verType?></td>
            </tr>
            <tr>
                <td align="right"><strong><i>3DS Security level used in the AUTH message: </i></strong></td>
                <td class="red"><?=$verSecurLevel?></td>
            </tr>
            <tr class="shade">
                <td align="right">
                    <strong><i>3DS CardHolder Enrolled: </strong>
                    <br />
                    <font size="1">Takes values: <strong>Y</strong> - Yes <strong>N</strong> - No</i></font>
                </td>
                <td class="red"><?=$enrolled?></td>
            </tr>
            <tr>
                <td align="right">
                    <i><strong>Authenticated Successfully: </strong><br />
                    <font size="1">Only returned if CardHolder Enrolled = <strong>Y</strong>. Takes values:<br />
                    <strong>Y</strong> - Yes <strong>N</strong> - No <strong>A</strong> - Attempted to Check <strong>U</strong> - Unavailable for Checking</font></i>
                </td>
                <td class="red"><?=$authStatus?></td>
            </tr>
            <tr class="shade">
                <td align="right"><strong><i>Payment Server 3DS Authentication Status Code: </i></strong></td>
                <td class="green"><?=$verStatus?></td>
            </tr>
            <tr>
                <td align="right"><i><strong>3DS Authentication Status Code Description: </strong></i></td>
                <td class="green"><?=getStatusDescription($verStatus)?></td>
            </tr>
            <tr>
                <td colspan="2" class="red" align="center">
                    <br />The 3-D Secure values shown in red are those values that are important values to store in case of future transaction repudiation.
                </td>
            </tr>
            <tr>
                <td colspan="2" class="green" align="center">
                    The 3-D Secure values shown in green are for information only and are not required to be stored.
                </td>
            </tr>
            <tr>
                <td colspan="2"><HR /></td>
            </tr>
        */
} ?>    </table>
    </body>
</html>
