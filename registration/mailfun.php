<?php
function sendMyConfimationMailPlease($to, $subject, $mailBody)
{
    require_once '../vendor/autoload.php';
    $mail = new PHPMailer;

    //$mail->SMTPDebug = 3;                              // Enable verbose debug output

    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'nilgiri.iiit.ac.in';                   // Specify main and backup SMTP servers
    $mail->SMTPAuth = false;                               // Enable SMTP authentication
    //$mail->Username = 'user@example.com';                 // SMTP username
    //$mail->Password = 'secret';                           // SMTP password
    $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
    $mail->SMTPAutoTLS = false;
    $mail->Port = 25;                                    // TCP port to connect to

    $mail->setFrom('iconnlp@iiit.ac.in', 'ICON2016 Registrations');
    $mail->addAddress($to);     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body    = str_replace("\n", "<br>\n", $mailBody);
    $mail->AltBody = str_replace("\n", "\r\n", $mailBody);
    $mail->send();
}

?>
