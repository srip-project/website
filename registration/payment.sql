-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 02, 2018 at 04:08 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `isoft_isec18`
--

-- --------------------------------------------------------

--
-- Table structure for table `credit_details`
--

CREATE TABLE IF NOT EXISTS `credit_details` (
  `email` varchar(50) NOT NULL DEFAULT '',
  `NAME` varchar(40) DEFAULT NULL,
  `NUMBER` varchar(16) DEFAULT NULL,
  `TYPE` varchar(10) DEFAULT NULL,
  `expir_month` varchar(10) DEFAULT NULL,
  `expir_year` int(11) DEFAULT NULL,
  `bill_address` varchar(250) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `country` varchar(40) DEFAULT NULL,
  `trans_date` date DEFAULT NULL,
  `amount` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `information`
--

CREATE TABLE IF NOT EXISTS `information` (
  `id` int NOT NULL AUTO_INCREMENT,
  `fname` varchar(1000) DEFAULT NULL,
  `lname` varchar(1000) DEFAULT NULL,
  `aff` varchar(1000) DEFAULT NULL,
  `position` varchar(1000) DEFAULT NULL,
  `regcountry` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `city` varchar(1000) DEFAULT NULL,
  `country` varchar(1000) DEFAULT NULL,
  `zip` varchar(1000) DEFAULT NULL,
  `phone` varchar(1000) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `currency`varchar(100) DEFAULT NULL,
  `regid` varchar(100) DEFAULT NULL,
  `payamount` varchar(100) DEFAULT NULL,
  `imgname` varchar(300) DEFAULT NULL,
  `imgtype` varchar(300) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `information`
--

-- --------------------------------------------------------

--
-- Table structure for table `late_pay`
--

CREATE TABLE IF NOT EXISTS `late_pay` (
  `del_name` varchar(250) DEFAULT NULL,
  `card_name` varchar(250) DEFAULT NULL,
  `card_no` varchar(25) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `persondetails`
--

CREATE TABLE IF NOT EXISTS `persondetails` (
  `transid` varchar(30) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `desig` varchar(1000) DEFAULT NULL,
  `place` varchar(1000) DEFAULT NULL,
  `affiliated` varchar(100) DEFAULT NULL,
  `Amount` varchar(30) DEFAULT NULL,
  `DATE` varchar(15) DEFAULT NULL,
  `MODE` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `persondetails`
--
-- --------------------------------------------------------

--
-- Table structure for table `requestvalues`
--

CREATE TABLE IF NOT EXISTS `requestvalues` (
  `transactionID` varchar(15) DEFAULT NULL,
  `DATE` varchar(20) DEFAULT NULL,
  `vpc_API_version` varchar(5) DEFAULT NULL,
  `Command` varchar(20) DEFAULT NULL,
  `Merchant_trans_ref` varchar(50) DEFAULT NULL,
  `Merchant_access_code` varchar(20) DEFAULT NULL,
  `Merchant_ID` varchar(20) DEFAULT NULL,
  `order_info` varchar(20) DEFAULT NULL,
  `purchase_amt` varchar(20) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `responsevalues`
--

CREATE TABLE IF NOT EXISTS `responsevalues` (
  `transactionID` varchar(100) DEFAULT NULL,
  `vpc_trans_response_code` varchar(5) DEFAULT NULL,
  `trans_response_code_desc` varchar(20) DEFAULT NULL,
  `message` varchar(50) DEFAULT NULL,
  `Receipt_number` varchar(20) DEFAULT NULL,
  `Transaction_number` varchar(20) DEFAULT NULL,
  `Acquirer_Response_Code` varchar(20) DEFAULT NULL,
  `Bank_authorizationID` varchar(20) DEFAULT NULL,
  `Batch_number` varchar(20) DEFAULT NULL,
  `Card_type` varchar(10) DEFAULT NULL,
  `amount` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `responsevalues`
--
-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `upload` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `imgname` varchar(255) NOT NULL,
  `imgsize` varchar(255) NOT NULL,
  `imgtype` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
   PRIMARY KEY (id)

) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `transID`
--

CREATE TABLE IF NOT EXISTS `transID` (
  `trans_no` int(10) NOT NULL,
  `vpc_url` varchar(50) DEFAULT NULL,
  `locale` varchar(15) DEFAULT NULL,
  `ticketno` varchar(20) DEFAULT NULL,
  `trans_source_subtype` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `credit_details`
--
ALTER TABLE `credit_details`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `transID`
--
ALTER TABLE `transID`
  ADD PRIMARY KEY (`trans_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transID`
--
ALTER TABLE `transID`
  MODIFY `trans_no` int(10) NOT NULL AUTO_INCREMENT;
COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
