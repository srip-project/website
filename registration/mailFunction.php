<?php
function sendMyConfimationMailPlease($to, $subject, $mailBody)
{
    require_once 'payment/mailer/PHPMailerAutoload.php';
    $mail = new PHPMailer;

    // $mail->SMTPDebug = 3;                              // Enable verbose debug output

     $mail->isSMTP();                                      // Set mailer to use SMTP
     $mail->Host = 'nilgiri.iiit.ac.in';                   // Specify main and backup SMTP servers
     $mail->SMTPAuth = false;                               // Enable SMTP authentication
    // $mail->SMTPAuth = true;                               // Enable SMTP authentication
    // $mail->Username = 'moodle@iiit.ac.in';                 // SMTP username
    // $mail->Password = 'password';                           // SMTP password
     $mail->SMTPSecure = '';                            // Enable TLS encryption, `ssl` also accepted
     $mail->SMTPAutoTLS = false;
     $mail->Port = 25;                                 // TCP port to connect to

    $mail->setFrom('smm18@iiit.ac.in', 'SMMW 2018 Registrations');
    $mail->addAddress($to);     // Add a recipient
    $mail->isHTML(true);                                  // Set email format to HTML

    $mail->Subject = $subject;
    $mail->Body    = str_replace("\n", "<br>\n", $mailBody);
    $mail->AltBody = str_replace("\n", "\r\n", $mailBody);
    $mail->send();
}

?>
