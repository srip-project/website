-- MySQL dump 10.14  Distrib 5.5.56-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: smmw2018
-- ------------------------------------------------------
-- Server version	5.5.56-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `credit_details`
--

DROP TABLE IF EXISTS `credit_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_details` (
  `email` varchar(50) NOT NULL DEFAULT '',
  `NAME` varchar(40) DEFAULT NULL,
  `NUMBER` varchar(16) DEFAULT NULL,
  `TYPE` varchar(10) DEFAULT NULL,
  `expir_month` varchar(10) DEFAULT NULL,
  `expir_year` int(11) DEFAULT NULL,
  `bill_address` varchar(250) DEFAULT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `country` varchar(40) DEFAULT NULL,
  `trans_date` date DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_details`
--

LOCK TABLES `credit_details` WRITE;
/*!40000 ALTER TABLE `credit_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `information`
--

DROP TABLE IF EXISTS `information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(1000) DEFAULT NULL,
  `lname` varchar(1000) DEFAULT NULL,
  `aff` varchar(1000) DEFAULT NULL,
  `position` varchar(1000) DEFAULT NULL,
  `regcountry` varchar(1000) DEFAULT NULL,
  `email` varchar(1000) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `city` varchar(1000) DEFAULT NULL,
  `country` varchar(1000) DEFAULT NULL,
  `zip` varchar(1000) DEFAULT NULL,
  `phone` varchar(1000) DEFAULT NULL,
  `amount` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `regid` varchar(100) DEFAULT NULL,
  `payamount` varchar(100) DEFAULT NULL,
  `imgname` varchar(300) DEFAULT NULL,
  `imgtype` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `information`
--

LOCK TABLES `information` WRITE;
/*!40000 ALTER TABLE `information` DISABLE KEYS */;
INSERT INTO `information` VALUES (1,'Guru','prasad','IIIT','ISCA','Student','j.guruprasad9@gmail.com','IIIT','Gachibowli','Iceland','7867654','7654356786','2000','INR','SMM18W1','2000','Screenshot from 2018-08-01 15-14-36.png','image/png'),(2,'GUGugu','jhkjh','IIII','ISCA','Delegate','adfdssfdf','asdsadsad','asdsadsa','United States','43657','53453443','5000','INR','SMM18W2','5000','Migration Certificate 10.pdf','application/pdf'),(3,'Venkata','Viraraghavan','Tata Consultancy Services','ISCA','Delegate','Venkatasubramanian.V@tcs.com','TCS','Bangalore','India','560066','9482218037','5000','INR','SMM18W3','5000','iscaMembership.jpg','image/jpeg'),(4,'Test','Test','Nowhere','Non_ISCA','Student','venkatasubramanian.v@tcs.com','888','BLR','India','560066','9482218037','2250','INR','SMM18W4','2250','iscaMembership.jpg','image/jpeg'),(5,'Lalit','Hissaria','TCS','Non_ISCA','Student','lalithissaria07@gmail.com','Bangalore','Bangalore','India','560066','9783907475','2250','INR','SMM18W5','2250','SEP.PNG','image/png'),(6,'GUutu','sffdg','sfdfdfdsf','Non_ISCA','Student','sdfsdfsd@sfsfsdf','dasdasdas','asdasdas','United States','asdasdas','asdad','2250','INR','SMM18W6','2250','Migration Certificate 10.pdf','application/pdf'),(7,'V','V','IITM','Non_Member','Student','venkatasubramanian.v@tcs.com','A','B','India','560066','9482218037','2250','INR','SMM18W7','2250','',''),(8,'V','V','TCS','Non_Member','Delegate','venkatasubramanian.v@tcs.com','A','B','Uganda','123456A','1284738934000','5500','INR','SMM18W8','5500','',''),(9,'V','V','IITM','ISCA','Student','venkatasubramanian.v@tcs.com','A','B','France','94830','33445566','2000','INR','SMM18W9','2000','VenkatV.jpg','image/jpeg'),(10,'v','v','venkatasubramanian.v@tcs.com','ISCA','Delegate','venkatasubramanian.v@tcs.com','a','b','Ireland','345345','45678900`11QWERWQER','5000','INR','SMM18W10','5000','VenkatV.jpg','image/jpeg');
/*!40000 ALTER TABLE `information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `late_pay`
--

DROP TABLE IF EXISTS `late_pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `late_pay` (
  `del_name` varchar(250) DEFAULT NULL,
  `card_name` varchar(250) DEFAULT NULL,
  `card_no` varchar(25) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `amount` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `late_pay`
--

LOCK TABLES `late_pay` WRITE;
/*!40000 ALTER TABLE `late_pay` DISABLE KEYS */;
/*!40000 ALTER TABLE `late_pay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persondetails`
--

DROP TABLE IF EXISTS `persondetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persondetails` (
  `transid` varchar(30) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `desig` varchar(1000) DEFAULT NULL,
  `place` varchar(1000) DEFAULT NULL,
  `affiliated` varchar(100) DEFAULT NULL,
  `Amount` varchar(30) DEFAULT NULL,
  `DATE` varchar(15) DEFAULT NULL,
  `MODE` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persondetails`
--

LOCK TABLES `persondetails` WRITE;
/*!40000 ALTER TABLE `persondetails` DISABLE KEYS */;
INSERT INTO `persondetails` VALUES ('SMM18W1','Guru prasad','ISCA','Student','IIIT','2000','04.08.18','INR'),('SMM18W2','GUGugu jhkjh','ISCA','Delegate','IIII','22','04.08.18','INR'),('SMM18W2','GUGugu jhkjh','ISCA','Delegate','IIII','2','04.08.18','INR'),('SMM18W3','Venkata Viraraghavan','ISCA','Delegate','Tata Consultancy Services','2','04.08.18','INR'),('SMM18W4','Test Test','Non_ISCA','Student','Nowhere','2','04.08.18','INR'),('SMM18W5','Lalit Hissaria','Non_ISCA','Student','TCS','2','04.08.18','INR'),('SMM18W6','GUutu sffdg','Non_ISCA','Student','sfdfdfdsf','2','04.08.18','INR'),('SMM18W7','V V','Non_Member','Student','IITM','2','04.08.18','INR'),('SMM18W8','V V','Non_Member','Delegate','TCS','2','04.08.18','INR'),('SMM18W9','V V','ISCA','Student','IITM','2','04.08.18','INR'),('SMM18W10','v v','ISCA','Delegate','venkatasubramanian.v@tcs.com','2','04.08.18','INR');
/*!40000 ALTER TABLE `persondetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestvalues`
--

DROP TABLE IF EXISTS `requestvalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requestvalues` (
  `transactionID` varchar(15) DEFAULT NULL,
  `DATE` varchar(20) DEFAULT NULL,
  `vpc_API_version` varchar(5) DEFAULT NULL,
  `Command` varchar(20) DEFAULT NULL,
  `Merchant_trans_ref` varchar(50) DEFAULT NULL,
  `Merchant_access_code` varchar(20) DEFAULT NULL,
  `Merchant_ID` varchar(20) DEFAULT NULL,
  `order_info` varchar(20) DEFAULT NULL,
  `purchase_amt` varchar(20) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestvalues`
--

LOCK TABLES `requestvalues` WRITE;
/*!40000 ALTER TABLE `requestvalues` DISABLE KEYS */;
/*!40000 ALTER TABLE `requestvalues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `responsevalues`
--

DROP TABLE IF EXISTS `responsevalues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsevalues` (
  `transactionID` varchar(100) DEFAULT NULL,
  `vpc_trans_response_code` varchar(5) DEFAULT NULL,
  `trans_response_code_desc` varchar(20) DEFAULT NULL,
  `message` varchar(50) DEFAULT NULL,
  `Receipt_number` varchar(20) DEFAULT NULL,
  `Transaction_number` varchar(20) DEFAULT NULL,
  `Acquirer_Response_Code` varchar(20) DEFAULT NULL,
  `Bank_authorizationID` varchar(20) DEFAULT NULL,
  `Batch_number` varchar(20) DEFAULT NULL,
  `Card_type` varchar(10) DEFAULT NULL,
  `amount` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsevalues`
--

LOCK TABLES `responsevalues` WRITE;
/*!40000 ALTER TABLE `responsevalues` DISABLE KEYS */;
INSERT INTO `responsevalues` VALUES ('SMM18W3','0','0','Approved','821619506953','2090000005','00','693506','20180804','VC',2),('SMM18W5','0','0','Approved','821620510541','2090000006','00','121513','20180804','MC',2),('','','','','','','','','','',0);
/*!40000 ALTER TABLE `responsevalues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transID`
--

DROP TABLE IF EXISTS `transID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transID` (
  `trans_no` int(10) NOT NULL AUTO_INCREMENT,
  `vpc_url` varchar(50) DEFAULT NULL,
  `locale` varchar(15) DEFAULT NULL,
  `ticketno` varchar(20) DEFAULT NULL,
  `trans_source_subtype` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`trans_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transID`
--

LOCK TABLES `transID` WRITE;
/*!40000 ALTER TABLE `transID` DISABLE KEYS */;
/*!40000 ALTER TABLE `transID` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upload`
--

DROP TABLE IF EXISTS `upload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upload` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `imgname` varchar(255) NOT NULL,
  `imgsize` varchar(255) NOT NULL,
  `imgtype` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upload`
--

LOCK TABLES `upload` WRITE;
/*!40000 ALTER TABLE `upload` DISABLE KEYS */;
/*!40000 ALTER TABLE `upload` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-10 15:40:50
