 <?php ?>
 <!DOCTYPE html>
 <html>
 <head>
     <title>Payment Page</title>
     <meta charset="utf-8">
   <link rel="icon" href="iiit-new.png" type="image/gif" sizes="16x16">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
 
  <link rel="stylesheet" type="text/css" href="/webiiit/collapse.css">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 </head>
    <style>
  body {
      font: 400 15px Lato, sans-serif;
      line-height: 1.2;
      color: #818181;
  }
  h2 {
      font-size: 24px;
      text-transform: uppercase;
      color: #303030;
      font-weight: 600;
      margin-bottom: 30px;
  }
  h4 {
      font-size: 25px;
      /*line-height: 1.375em;*/
      color: #303030;
      font-weight: 400;
      margin-bottom: 20px;
  }  
  .jumbotron {
      background-color: #adccd2;
      color: #fff;
      padding: 100px 25px;
      font-family: Montserrat, sans-serif;
  }
  .container-fluid {
      padding: 60px 50px;
  }
  .bg-grey {
      background-color: #f6f6f6;
  }
  .logo-small {
      color: #f4511e;
      font-size: 50px;
  }
  .logo {
      color: #f4511e;
      font-size: 200px;
  }
  .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
  }
  .thumbnail img {
      width: 100%;
      height: 100%;
      margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
      background-image: none;
      color: #f4511e;
  }
  .carousel-indicators li {
      border-color: #f4511e;
  }
  .carousel-indicators li.active {
      background-color: #f4511e;
  }
  .item h4 {
      font-size: 19px;
      line-height: 1.375em;
      font-weight: 400;
      font-style: italic;
      margin: 70px 0;
  }
  .item span {
      font-style: normal;
  }
  .panel {
      border: 1px solid #f4511e; 
      border-radius:0 !important;
      transition: box-shadow 0.5s;
  }
  .panel:hover {
      box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
      border: 1px solid #f4511e;
      background-color: #fff !important;
      color: #f4511e;
  }
  .panel-heading {
      color: #fff !important;
      background-color:#757575 !important;
      padding: 25px;
      border-bottom: 1px solid transparent;
      border-top-left-radius: 0px;
      border-top-right-radius: 0px;
      border-bottom-left-radius: 0px;
      border-bottom-right-radius: 0px;
  }
  .panel-footer {
      background-color: white !important;
  }
  .panel-footer h3 {
      font-size: 32px;
  }
  .panel-footer h4 {
      color: #aaa;
      font-size: 14px;
  }
  .panel-footer .btn {
      margin: 15px 0;
      background-color: #f4511e;
      color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: cadetblue;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;

    line-height: 2.428571 !important;
    letter-spacing: 2px;
    border-radius: 0;
    font-family: Candara;
}
  
  .navbar li a, .navbar .navbar-brand {
      color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
      color: #f4511e !important;
      background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
  }
  footer .glyphicon {
      font-size: 20px;
      margin-bottom: 20px;
      color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
      animation-name: slide;
      -webkit-animation-name: slide;
      animation-duration: 1s;
      -webkit-animation-duration: 1s;
      visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
        width: 100%;
        margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
        font-size: 150px;
    }
  }

  .text{
    font-size: 20px;
    font-family: Cambria;
    text-align: right;

  }

  .text1 {
    font-size: 40px;
    font-family: Candara;
    color: #ff5722;
}

.text2{
   font-size: 25px;
    font-family: Candara;
    color:black;


  }

  .text3{
    font-family: Candara;
    font-size:20px;
    color: black;
  }

  .text4{
    font-family: Candara;
    font-size: 30px;
    color: black;
  }

  .text5 {
    font-size: 20px;
    color: black;
  }

  .text6 {
    font-family: Candara;
    font-size:20px;
    color: green;

  }

  .text7{
     font-size: 22px;
    color: green;
    font-family: Candara;


  }

  .text8{
    font-size: 20px;
    color: green;
    font-family: Candara;


  }
  </style>
}
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.html">USMCA 2018</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
              
        <li><a href="#Payment"><b>Payment Portal</b></a></li>
        
        
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center">
     <!--  <h4 class="text"><i>First Announcement & Call for Abstracts</i></h4> -->
      <span class="text1">USMCA 2018</span><br>
      <span class="text2"><b>17<sup>th</sup> INTERNATIONAL SYMPOSIUM</b></span><br>
      <span class="text3">On</span><br>
      <span class="text4"><b>NEW TECHNOLOGIES FOR URBAN SAFETY OF MEGA CITIES IN ASIA</b></span><br>
      <span class="text5"><i>12-14 December 2018, Hyderabad, India </i> </span><br><br><br>
      <span class="text6">Organized by</span><br>
      <span class="text7"><b> International Institute of Information Technology, Hyderabad</b></span><br>
      <span class="text8">and</span><br>
      <span class="text7"><b>International Center for Urban Safety Engineering, 
Institute of Industrial Science, The University of Tokyo, JAPAN</b></span><br><br>

<img src="iiith1.png"> &nbsp; &nbsp; &nbsp; &nbsp; <img src="icus_logo.png" height="150" width="150">
 


       


  
  
</div> 





 <body>

    <div id="Payment" class="container-fluid text-center">
        <h2>Payment Details and Payment Channel</h2>
   <div class="panel-body">
  

    <form class="form-horizontal"  action="xxx.php" method="post" autocomplete="off">
      
    <div class="form-group">
      <label class="control-label col-sm-2" for="name">Registration ID: </label>
       <div class="col-sm-8">          
        <input class="form-control" id="pwd" placeholder="First or Given Name" name="name" type="text" required>
      </div>
    
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Name :</label>
      <div class="col-sm-8">          
        <input class="form-control" id="pwd" placeholder="Last Name" name="lname" type="text" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Amount to be paid :</label>
      <div class="col-sm-8">          
        <input class="form-control" id="pwd" placeholder="Affiliation" name="aff" type="text" required>
      </div>
    </div>
    
    
    
    
    
 
      
   
    
    
    
   
   

   <div class="col-md-10 text-right">
   <button class="btn btn-primary" name="btn-signup">Submit</a>
   <!-- <button type="submit" class="btn btn-primary" name="btn-signup">Submit</button> -->
   <!--  <button type="button" class="btn btn-primary">Offline Payment</button> -->
  </div>
    
    </form>
 </div>
 </body>
 </html>


 